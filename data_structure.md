# file data struct

```json
{
  "appVersion": "0.0.1",
  "appVersionCode": 1, // app version code,update when application updated
  "versionCode": 2, // increase when update
  "masterCodeHistory": [], // support to calc history password [use select]
  "masterCode": "20190521", // default, application start date, will set at first use
  "tags": [
    {
      "id": "xxxx", // random str
      "name": "login"
    }
  ],
  "records": [
    {
      "id": "xxxx", // random str
      "name": "appleid",
      "tagIds": ["tagid"],
      "createdAt": 1564581816,
      "updatedAt": 1564581816,
      "account": "yoshi", // unique, if changed, gen new record
      "history": [{ "hid": "xxxxxx", "len": 16, "type": [number, char, punctuation] }], // history random str for regen password，part of password
      "extInfo": {
        "url": "https://www.google.com",
        "email": "939926954@qq.com",
        "remark": "awesome"
      }
    }
  ]
}
```

