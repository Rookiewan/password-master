# local data and update

> Use [lovafield](https://github.com/google/lovefield) to save data at local to improve application



### scene one: application load at first time

1. Download remote data
2. Create local database and save it



### scene two: application load later

1. use local data first

2. Fetch remote data and update

   1. compare version code(which increase it self after update remote data)
   2. if remote is updated, then update local

   

### notice

1. Check remote data's version before update