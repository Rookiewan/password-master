# pasword-master

> 只实现网页版本，托管在 github 上

personal password use



## 实现思路

> 不借助服务器，通过 **账户名（默认空）** + **网站标识（可选，可用于网页识别网站）** + **主密码** + **算法** + **远程固定密码（json，保存网站等信息）** 完全在客户端计算实时得出密码

- 先进行 one drive 授权，保存 token
- 同步 one drive 文件信息，根据时间戳或版本号
- 可输入**账户名**和**主密码**实时算出密码（由于存在更换密码，提供备注可填选项，匹配账户名和密码时列出基于备注的匹配可选项）



## 技术调研

#### 本地存储

使用 **[lovefield](https://github.com/google/lovefield)** 类数据库语法

#### 远程固定（密码）文件

基于 **one drive** 授权后进行 唯一文件 上传、下载。（模拟仅有一张表的远程数据库）

#### 响应式布局

使用 **[pure](https://github.com/pure-css/pure)** css 框架
中文文档 **[https://www.purecss.cn/](https://www.purecss.cn/)**





## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```

### Run your tests
```
yarn run test
```

### Lints and fixes files
```
yarn run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
