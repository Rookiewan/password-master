import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import dataManager from './js/dataManager'
import fileSync from './utils/fileSync'
import 'purecss'
import './css/global.scss'
import flexible from 'rem-flexible'
import Snotify from 'vue-snotify'

import 'vue2-animate/dist/vue2-animate.min.css'

flexible(750, 100)

Vue.use(Snotify)

dataManager.use(fileSync)

// TODO: git commit rules

Vue.config.productionTip = false
Vue.prototype.$dataManager = dataManager

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
