import Vue from 'vue'
import isMobile from 'is-mobile'
import Router from 'vue-router'
import Index from './views/Index.vue'
import IndexMobile from './views/mobile/Index.vue'

Vue.use(Router)

export default new Router({
  mode: 'hash',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'index',
      component: (isMobile() || process.env.isMiniprogram) ? IndexMobile : Index
    }
    // {
    //   path: '/about',
    //   name: 'about',
    //   // route level code-splitting
    //   // this generates a separate chunk (about.[hash].js) for this route
    //   // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ]
})
