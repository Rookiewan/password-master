/* eslint-disable import/prefer-default-export */
// eslint-disable-next-line import/no-mutable-exports
let localStorageKeys = {
  USER_TOKEN: 'USER_TOKEN',
  LOGIN_INFO: 'LOGIN_INFO'
}


function addPrefix(localStorageKeysParam, prefix = '_PASSWORD_MASTER_') {
  Object.keys(localStorageKeysParam).forEach((k) => {
    // eslint-disable-next-line no-param-reassign
    localStorageKeysParam[k] = prefix + localStorageKeysParam[k]
  })
  return localStorageKeys
}

localStorageKeys = addPrefix(localStorageKeys)

export {
  localStorageKeys
}
