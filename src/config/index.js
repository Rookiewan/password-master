const msalConfig = {
  auth: {
    clientId: '35c89380-0508-434c-b2b2-28476d0a629f',
    authority: 'https://login.microsoftonline.com/common'
  },
  cache: {
    cacheLocation: 'localStorage',
    storeAuthStateInCookie: true
  }
}
const app = {
  development: {
    name: 'password-master-dev'
  },
  production: {
    name: 'password-master'
  }
}
const defaultFileContent = {
  appVersion: '0.0.1',
  appVersionCode: 1,
  versionCode: 2, // TODO: update basic struct when versioCode change
  masterCodeHistory: [],
  masterCode: '20190521', // default, application start date, will set at first use
  tags: [],
  records: []
}

const config = {
  msalConfig,
  // eslint-disable-next-line no-undef
  app: app[process.env.NODE_ENV],
  defaultFileContent
}

export default config
