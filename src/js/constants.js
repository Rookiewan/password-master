export const PM_MASTER_CODE_KEY = 'PM_MASTER_CODE_KEY'
export const PM_LOCAL_MASTER_CODE_KEY = 'PM_LOCAL_MASTER_CODE_KEY'
export const PM_REMOTE_DATA = 'PM_REMOTE_DATA' // except records
export const PM_HIDE_LOCAL_MASTER_CODE_KEY = 'PM_HIDE_LOCAL_MASTER_CODE_KEY'

// numbers
export const NUMBERS = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9']

// lower case chars
export const LOWER_CASE_CHARS = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

// upper case chars
export const UPPER_CASE_CHARS = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z']

// normal punctuation
export const NORMAL_PUNCTUATION = ['.', '_', ',', '!', '*', '#', '%', '$', '&', '+', '-', '/', '>', '=', '<', '?', '@', '[', ']', '|', '{', '}', '~', ';', ':']

// ext info key
export const EXT_INFO = [
  { key: 'url', default: '' },
  { key: 'email', default: '' },
  { key: 'remark', default: '' }
]
