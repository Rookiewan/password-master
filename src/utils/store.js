import engine from 'store/src/store-engine'
import localStorage from 'store/storages/localStorage'
import sessionStorage from 'store/storages/sessionStorage'
import defaultPlugin from 'store/plugins/defaults'
import expirePlugin from 'store/plugins/expire'

const plugins = [defaultPlugin, expirePlugin]

export const localStore = engine.createStore([localStorage], plugins)
export const sessionStore = engine.createStore([sessionStorage], plugins)
