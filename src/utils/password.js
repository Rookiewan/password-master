// gen password
class Password {
  constructor({ id = null, tagIds = [], name = '', username = '', history = [], extInfo = '' }) {
    this.struct = {
      id,
      name,
      tagIds,
      createdAt: '',
      updatedAt: '',
      username: '',
      history,
      extInfo: {
        url: '',
        email: '',
        remark: ''
      }
    }
  }
}
