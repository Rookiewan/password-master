import Vue from 'vue'
import { sync } from 'vuex-router-sync'
import App from '../../App.vue'
import store from '../../store'
import router from '../../router'
import dataManager from '../../js/dataManager'
// import fileSync from '../../utils/fileSync'
import 'purecss'
import '../../css/global.scss'
import Snotify from 'vue-snotify'

import 'vue2-animate/dist/vue2-animate.min.css'

Vue.use(Snotify)

// dataManager.use(fileSync)

// TODO: git commit rules

Vue.config.productionTip = false
Vue.prototype.$dataManager = dataManager

export default function createApp() {
  const container = document.createElement('div')
  container.id = 'app'
  document.body.appendChild(container)

  Vue.config.productionTip = false

  sync(store, router)

  window.onload = () => {
    if (process.env.isMiniprogram) {
      // 小程序
      document.documentElement.style.fontSize = `${(wx.getSystemInfoSync().screenWidth * 100) / 750}px`
    }
  }

  return new Vue({
    el: '#app',
    router,
    store,
    render: h => h(App)
  })
}
